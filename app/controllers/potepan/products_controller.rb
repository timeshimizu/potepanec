class Potepan::ProductsController < ApplicationController
  PRODUCTS_LIMIT_MAX_NUMBER = 4
  def show
    @product = Spree::Product.find(params[:id])
    @taxon = @product.taxons.first
    @products = @product.related_products.includes(master: [:images, :default_price]).
      limit(PRODUCTS_LIMIT_MAX_NUMBER)
  end
end
