require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    context "タイトルがnilの時" do
      it "タイトルはbasetitleのみ" do
        expect(full_title(nil)).to eq 'BIGBAG store'
      end
    end

    context "タイトルが空の時" do
      it "タイトルはbasetitleのみ" do
        expect(full_title("")).to eq 'BIGBAG store'
      end
    end

    context "タイトルが与えられた時" do
      it "タイトルは、与えられたものとbasetitleを合わせたもの" do
        expect(full_title("SINGLE product")).to eq 'SINGLE product | BIGBAG store'
      end
    end
  end
end
