require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "get /potepan/categories" do
    let(:taxonomy) { create(:taxonomy, name: "Categories") }
    let(:taxon1)   { create(:taxon, name: "Bags", taxonomy: taxonomy) }
    let(:taxon2)   { create(:taxon, name: "Mugs", taxonomy: taxonomy) }
    let!(:product) { create(:product, name: "Tote bug", taxons: [taxon1]) }

    before do
      get potepan_category_path(taxon1.id)
    end

    it "レスポンスが正しい" do
      expect(response).to be_successful
    end

    it "@taxonomiesが正しく表示される" do
      expect(response.body).to include taxonomy.name
    end

    it "@taxonが正しく表示される" do
      expect(response.body).to include taxon1.name
    end

    it "@productが正しく表示される" do
      expect(response.body).to include product.name
    end
  end
end
