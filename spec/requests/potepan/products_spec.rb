require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "get /potepan/products" do
    let(:taxonomy)          { create(:taxonomy, name: "t") }
    let(:root)              { taxonomy.root }
    let(:taxon1)            { create(:taxon, taxonomy: taxonomy, parent: root) }
    let(:taxon2)            { create(:taxon, taxonomy: taxonomy, parent: root) }
    let(:product)           { create(:product, taxons: [taxon1]) }
    let!(:releted_product1) { create(:product, price: "111", taxons: [taxon1]) }
    let!(:releted_product2) { create(:product, price: "222", taxons: [taxon1]) }
    let!(:releted_product3) { create(:product, price: "333", taxons: [taxon1]) }
    let!(:releted_product4) { create(:product, price: "444", taxons: [taxon1]) }
    let!(:releted_product5) { create(:product, price: "555", taxons: [taxon1]) }
    let!(:other_product)    { create(:product, price: "666", taxons: [taxon2]) }

    before do
      get potepan_product_path(product.id)
    end

    it "レスポンスが正しい" do
      expect(response).to be_successful
    end

    it "@productが正しく表示される" do
      expect(response.body).to include product.name
    end

    it "@taxonが正しく表示される" do
      expect(response.body).to include potepan_category_path(taxon1.id)
    end

    it "4つ目の関連商品が表示される" do
      expect(response.body).to include "444"
    end

    it "5つ目の関連商品が表示されない" do
      expect(response.body).not_to include "555"
    end

    it "違うtaxonの商品が表示されない" do
      expect(response.body).not_to include "666"
    end
  end
end
