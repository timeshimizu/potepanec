require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "タイトルが表示される" do
    expect(page).to have_title "#{product.name} | BIGBAG store"
    within ".page-title" do
      expect(page).to have_content product.name
    end
  end

  scenario "関連商品に、そのページのメイン商品が含まれない" do
    within ".productBox" do
      expect(page).not_to have_content product.name
    end
  end

  scenario "一覧ページに正しく戻れる" do
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario "関連商品画像をクリックした時正しく詳細ページに飛ぶ" do
    click_link related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
    expect(page).to have_content related_product.name
    expect(page).to have_content related_product.display_price
  end
end
